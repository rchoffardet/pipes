﻿namespace Rchoffardet.Pipes;

public interface IPipeStep<TIn, TOut> : IPipeStepHooks<TIn, TOut>
{
    PipeStepResult<TOut> Process(TIn input);
}


public interface IPipeStepHooks<TIn, TOut>
{
    void BeforeStep(TIn input);
    void AfterSuccess(TIn input, TOut output);
    void AfterFailure(TIn input, PipeStepResult<TOut> result);
}

public abstract class PipeStep<TIn, TOut> : IPipeStep<TIn, TOut>
{
    public virtual void BeforeStep(TIn input)
    { }

    public virtual void AfterSuccess(TIn input, TOut output)
    { }

    public virtual void AfterFailure(TIn input, PipeStepResult<TOut> result)
    { }

    public abstract PipeStepResult<TOut> Process(TIn input);
}