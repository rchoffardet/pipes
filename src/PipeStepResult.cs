﻿namespace Rchoffardet.Pipes;

public readonly struct PipeStepResult<T>
{
    public bool Succeed { get; }
    public bool Failed => !Succeed;
    private readonly T? _result;

    public PipeStepResult(T? result)
    {
        _result = result;
        Succeed = true;
    }

    public PipeStepResult()
    {
        Succeed = false;
        _result = default(T);
    }

    public T Value =>
        Succeed
            ? _result!
            : throw new Exception("Value has failed and thus has no result.");

    public async Task<PipeStepResult<U>> Bind<U>(Func<T, Task<PipeStepResult<U>>> selector)
    {
        if (Succeed)
        {
            return await selector(_result!);
        }

        return PipeStepResult.Failed<U>();
    }

    public PipeStepResult<U> Bind<U>(Func<T, PipeStepResult<U>> selector)
    {
        if (Succeed)
        {
            return selector(_result!);
        }

        return PipeStepResult.Failed<U>();
    }
}

public static class PipeStepResult
{
    public static PipeStepResult<T> Ok<T>(T result)
    {
        return new PipeStepResult<T>(result);
    }

    public static PipeStepResult<T> Failed<T>()
    {
        return new PipeStepResult<T>();
    }
}