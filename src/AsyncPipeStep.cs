﻿namespace Rchoffardet.Pipes;

public interface IAsyncPipeStep<TIn, TOut> : IAsyncPipeStepHooks<TIn, TOut>
{
    Task<PipeStepResult<TOut>> Process(TIn input, CancellationToken token);
}

public interface IAsyncPipeStepHooks<TIn, TOut>
{
    void BeforeStep(TIn input, CancellationToken token);
    void AfterSuccess(TIn input, TOut output, CancellationToken token);
    void AfterFailure(TIn input, PipeStepResult<TOut> result, CancellationToken token);
}

public abstract class AsyncPipeStep<TIn, TOut> : IAsyncPipeStep<TIn, TOut>
{
    public virtual void BeforeStep(TIn input, CancellationToken token)
    { }

    public virtual void AfterSuccess(TIn input, TOut output, CancellationToken token)
    { }

    public virtual void AfterFailure(TIn input, PipeStepResult<TOut> result, CancellationToken token)
    { }

    public abstract Task<PipeStepResult<TOut>> Process(TIn input, CancellationToken token);
}