﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace Rchoffardet.Pipes;

public class Pipe<TIn, TOut>
{
    private readonly Func<TIn, CancellationToken, Task<PipeStepResult<TOut>>> _step;

    public Pipe(IAsyncPipeStep<TIn, TOut> step)
        : this(step.Process, step)
    { }

    public Pipe(Func<TIn, CancellationToken, Task<PipeStepResult<TOut>>> innerStep, IAsyncPipeStepHooks<TIn, TOut>? hooks)
    : this(CreateStepAsync(innerStep, hooks))
    { }

    private Pipe(Func<TIn, CancellationToken, Task<PipeStepResult<TOut>>> step)
    {
        _step = step;
    }

    public Pipe<TIn, TNewOut> Chain<TNewOut>(
        Func<TOut, CancellationToken, Task<PipeStepResult<TNewOut>>> nextInnerStep, 
        IAsyncPipeStepHooks<TOut, TNewOut>? hooks = null
    )
    {
        var nextStep = CreateStepAsync(nextInnerStep, hooks);

        return new Pipe<TIn, TNewOut>(async (x, token) =>
        {
            var result = await _step(x, token);
            return await result.Bind(r => nextStep(r, token));
        });
    }

    public Pipe<TIn, TNewOut> Chain<TNewOut>(
        Func<TOut, PipeStepResult<TNewOut>> nextInnerStep,
        IPipeStepHooks<TOut, TNewOut>? hooks = null
    )
    {
        var nextStep = CreateStep(nextInnerStep, hooks);

        return new Pipe<TIn, TNewOut>(async (x, token) =>
        {
            var result = await _step(x, token);
            return result.Bind(nextStep);
        });
    }

    public Pipe<TIn, TNewOut> Chain<TNewOut>(IAsyncPipeStep<TOut, TNewOut> step)
    {
        return Chain(step.Process, step);
    }

    public Pipe<TIn, TNewOut> Chain<TNewOut>(IPipeStep<TOut, TNewOut> step)
    {
        return Chain(step.Process, step);
    }

    public async Task<PipeStepResult<TOut>> Run(TIn input, CancellationToken token)
    {
        return await _step(input, token);
    }

    private static Func<TNewIn, CancellationToken, Task<PipeStepResult<TNewOut>>> CreateStepAsync<TNewIn, TNewOut>(
        Func<TNewIn, CancellationToken, Task<PipeStepResult<TNewOut>>> innerStep, 
        IAsyncPipeStepHooks<TNewIn, TNewOut>? hooks
    )
    {
        return async (input, token) =>
        {
            if (token.IsCancellationRequested)
            {
                return PipeStepResult.Failed<TNewOut>();
            }

            hooks?.BeforeStep(input, token);

            var output = await innerStep(input, token);
            if (output.Succeed)
            {
                hooks?.AfterSuccess(input, output.Value, token);
            }
            else
            {
                hooks?.AfterFailure(input, output, token);
            }

            return output;
        };
    }

    private static Func<TNewIn, PipeStepResult<TNewOut>> CreateStep<TNewIn, TNewOut>(
        Func<TNewIn, PipeStepResult<TNewOut>> innerStep,
        IPipeStepHooks<TNewIn, TNewOut>? hooks
    )
    {
        return (input) =>
        {
            hooks?.BeforeStep(input);

            var output = innerStep(input);
            if (output.Succeed)
            {
                hooks?.AfterSuccess(input, output.Value);
            }
            else
            {
                hooks?.AfterFailure(input, output);
            }

            return output;
        };
    }
}

public static class Pipe
{
    public static Pipe<TIn, TIn> StartWith<TIn>()
    {
        // TODO: find a syntactic sugar absolutely no overhead
        return new Pipe<TIn, TIn>(async (x, token) => PipeStepResult.Ok(x), null); 
    }

    public static Pipe<TIn, TOut> StartWith<TIn, TOut>(IAsyncPipeStep<TIn, TOut> step)
    {
        return new Pipe<TIn, TOut>(step);
    }

    public static Pipe<TIn, TOut> StartWith<TIn, TOut>(Func<TIn, CancellationToken, Task<PipeStepResult<TOut>>> step)
    {
        return new Pipe<TIn, TOut>(step, null);
    }

    public static Pipe<TIn, TOut> StartWith<TIn, TOut>(Func<TIn, CancellationToken, Task<PipeStepResult<TOut>>> step, IAsyncPipeStepHooks<TIn, TOut> asyncPipeStepHooks)
    {
        return new Pipe<TIn, TOut>(step, asyncPipeStepHooks);
    }
}
