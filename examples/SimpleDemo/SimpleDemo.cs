﻿using Rchoffardet.Pipes;

namespace examples.SimpleDemo;

class SimpleDemo
{
    public async Task Run()
    {
        try
        {
            var pipe = GetPipe();
            var source = new CancellationTokenSource(TimeSpan.FromSeconds(3));
            var token = source.Token;
            var result = pipe.Run(1337, token);
            Console.WriteLine((await result).Value);
        }
        catch (Exception)
        {
            Console.WriteLine("Canceled");
            throw;
        }
    }

    public Pipe<int, int> GetPipe()
    {
        return Pipe
            .StartWith<int>()
            .Chain(Steps.MultiplyBy42)
            .Chain(Steps.ToString)
            .Chain(Steps.CountLength);
    }

}

static class Steps
{
    public static async Task<PipeStepResult<int>> MultiplyBy42(int x, CancellationToken token)
    {
        await Task.Delay(1000, token);
        return PipeStepResult.Ok(x * 42);
    }

    public static PipeStepResult<string> ToString(int x)
    {
        return PipeStepResult.Ok($"{x}");
    }

    public static async Task<PipeStepResult<int>> CountLength(string x, CancellationToken token)
    {
        await Task.Delay(1000, token);
        return PipeStepResult.Ok(x.Length);
    }
}