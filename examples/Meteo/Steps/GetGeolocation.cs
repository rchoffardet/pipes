﻿using Windows.Devices.Geolocation;
using Rchoffardet.Pipes;

namespace examples.Meteo.Steps
{
    class GetGeolocation : AsyncPipeStep<Unit, Geoposition>
    {
        public override async Task<PipeStepResult<Geoposition>> Process(Unit input, CancellationToken token)
        {
            var accessStatus = await Geolocator.RequestAccessAsync();
            switch (accessStatus)
            {
                case GeolocationAccessStatus.Allowed:
                    var locator = new Geolocator();
                    var pos = await locator.GetGeopositionAsync();

                    if (pos == null)
                    {
                        return PipeStepResult.Failed<Geoposition>();
                    }

                    return PipeStepResult.Ok(pos);

                default:
                case GeolocationAccessStatus.Denied:
                case GeolocationAccessStatus.Unspecified:
                    return PipeStepResult.Failed<Geoposition>();
            }
        }

        public override void AfterFailure(Unit input, PipeStepResult<Geoposition> result, CancellationToken token)
        {
            Console.Write("Formatting failed.");
        }
    }
}
