﻿using Rchoffardet.Pipes;

namespace examples.Meteo.Steps
{
    internal class FormatWeather : PipeStep<Weather,string>
    {
        public override PipeStepResult<string> Process(Weather input)
        {
            return PipeStepResult.Ok(
                $"""
                Temps: {GetWeatherDescription(input.code)}
                Température: {input.temperature}°C
                Vent: {input.windSpeed}km/h {GetWindDirection(input.windDirection)}
                """
            );
        }

        private string GetWindDirection(float dir)
        {
            return (dir % 360) switch
            {
                >= 337.5f or < 22.5f => "Nord",
                >= 22.5f and < 67.5f => "Nord-Est",
                >= 67.5f and < 112.5f => "Est",
                >= 112.5f and < 157.5f => "Sud-Est",
                >= 157.5f and < 202.5f => "Sud",
                >= 202.5f and < 247.5f => "Sud-Ouest",
                >= 247.5f and < 292.5f => "Ouest",
                >= 292.5f and < 337.5f => "Nord-Ouest",
                _ => "Direction inconnue"
            };
        }

        private string GetWeatherDescription(byte code)
        {
            return code switch
            {
                0 => "Ciel dégagé",
                1 or 2 or 3 => "Principalement clair, partiellement nuageux et couvert",
                45 or 48 => "Brouillard et brouillard de givre",
                51 or 53 or 55 => "Bruine : Légère, modérée et intense",
                56 or 57 => "Bruine verglaçante : Légère et intense",
                61 or 63 or 65 => "Pluie : Légère, modérée et forte",
                66 or 67 => "Pluie verglaçante : Légère et forte",
                71 or 73 or 75 => "Chute de neige : Légère, modérée et forte",
                77 => "Grains de neige",
                80 or 81 or 82 => "Averses de pluie : Légères, modérées et violentes",
                85 or 86 => "Averses de neige : Légères et fortes",
                95 => "Orage : Léger ou modéré",
                96 or 99 => "Orage avec grêle : Léger et fort",
                _ => "Description indisponible"
            };
        }

        public override void AfterFailure(Weather input, PipeStepResult<string> result)
        {
            Console.Write("GetGeolocation failed.");
        }
    }
}
