﻿using System.Globalization;
using System.Net.Http.Json;
using Windows.Devices.Geolocation;
using Rchoffardet.Pipes;

namespace examples.Meteo.Steps;

class GetWeather : AsyncPipeStep<Geoposition, Weather>
{
    private readonly IHttpClientFactory factory;
    private const string baseUrl = "https://api.open-meteo.com/v1/forecast";
        
    public GetWeather(IHttpClientFactory factory)
    {
        this.factory = factory;
    }

    public override async Task<PipeStepResult<Weather>> Process(Geoposition input, CancellationToken token)
    {
        var client = factory.CreateClient();
        client.Timeout = TimeSpan.FromMilliseconds(500);

        var coords = input.Coordinate;

        try
        {
            var latitude = $"latitude={coords.Latitude.ToString(CultureInfo.InvariantCulture)}";
            var longitude = $"longitude={coords.Longitude.ToString(CultureInfo.InvariantCulture)}";
            var url = $"{baseUrl}?{latitude}&{longitude}&current_weather=true";

            var result = await client.GetFromJsonAsync<WeatherResult>(url, token);

            if (result == null)
            {
                return PipeStepResult.Failed<Weather>();
            }

            return PipeStepResult.Ok(result.current_weather!);
        }
        catch (Exception)
        {
            return PipeStepResult.Failed<Weather>();
        }
    }

    public override void AfterFailure(Geoposition input, PipeStepResult<Weather> result, CancellationToken token)
    {
        Console.Write("GetWeather failed.");
    }
}

public record WeatherResult(Weather current_weather);

public class Weather
{
    public float temperature { get; set; }
    public byte code { get; set; }
    public float windSpeed { get; set; }
    public float windDirection { get; set; }
}