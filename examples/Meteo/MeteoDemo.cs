﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using examples.Meteo.Steps;
using Microsoft.Extensions.DependencyInjection;
using Rchoffardet.Pipes;

namespace examples.Meteo
{
    public static class MeteoDemo
    {
        public static async Task Run()
        {
            var source = new CancellationTokenSource(TimeSpan.FromSeconds(3));
            var token = source.Token;

            var pipe = GetPipe();
            var result = await pipe.Run(Unit.unit, token);

            if (result.Succeed)
            {
                Console.WriteLine(result.Value);
            }
            else
            {
                Console.WriteLine("Failure");
            }
        }

        private static Pipe<Unit, string> GetPipe()
        {
            var services = new ServiceCollection();
            services.AddHttpClient();
            var serviceProvider = services.BuildServiceProvider();

            return Pipe
                .StartWith<Unit>()
                .Chain(new GetGeolocation())
                .Chain(new GetWeather(serviceProvider.GetRequiredService<IHttpClientFactory>()))
                .Chain(new FormatWeather());
        }
    }
}
